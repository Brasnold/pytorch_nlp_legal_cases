# Pytorch NLP: legal case reports summarization

**Dati**
* <a href=http://www.aclweb.org/anthology/W04-1907>The HOLJ Corpus</a>

**Ricerche**
* <a href=https://arxiv.org/pdf/1804.09321.pdf>Hierarchical RNN for Information Extraction from
Lawsuit Documents</a>

**Test examples**

Il database utilizzato è costituito da **68 documenti**, 
relativemente brevi, manualmente annotati. 

* **56 documenti** sono stati usati come **training set**
* **8 documenti** sono stati usati come **validation set**
* **4 documenti** sono stati usati come **test set**

Sul training il risultato conseguito è: **accuratezza 68%**.
Nella cartella **results** è possibile visualizzare in formato *html* 
i **4 test examples**.

![](img/esempio.png)