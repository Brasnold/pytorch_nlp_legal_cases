from path import Path


PROJECT_ROOT_DIR = Path(__file__).parent

MODEL_DIR = PROJECT_ROOT_DIR / 'model'
MODEL_DIR.mkdir_p()

MODEL_PATH = MODEL_DIR / 'model.pt'

RESULTS_DIR = PROJECT_ROOT_DIR / 'results'
RESULTS_DIR.mkdir_p()

DATA_DIR = PROJECT_ROOT_DIR / 'data'
DATA_DIR.mkdir_p()

RAW_DATA_DIR = DATA_DIR / 'raw'
RAW_DATA_DIR.mkdir_p()

PROC_DATA_DIR = DATA_DIR / 'processed'
PROC_DATA_DIR.mkdir_p()

TRAIN_DATA_PATH = PROC_DATA_DIR / 'train.pkl'

VALID_DATA_PATH = PROC_DATA_DIR / 'validation.pkl'

TEST_DATA_PATH = PROC_DATA_DIR / 'test.pkl'

GLOVE_PATH = DATA_DIR / 'glove.6B.300d.txt'

EMBEDDING_INIT_PATH = DATA_DIR / 'embedding_init.pkl'
VOCABULARY_PATH = DATA_DIR / 'vocabulary.pkl'