label_to_index = {'FACT': 0,
                  'PROCEEDINGS': 1,
                  'BACKGROUND': 2,
                  'FRAMING': 3,
                  'DISPOSAL': 4,
                  'TEXTUAL': 5,
                  'OTHER': 6
                  }

index_to_label = {v:k for k, v in label_to_index.items()}

n_class = 7

unk_token = '<UNK>'
mask_token = '<MASK>'
eos_token = '<EOS>'
bos_token = '<BOS>'