import spacy
import re
import os
from path import Path
from preprocessing.document import Document
from tqdm import tqdm
from preprocessing.glove import Glove
from constants import label_to_index, mask_token, eos_token, bos_token
from preprocessing.vocabulary import SpecialVocabulary
from torchmodel.embedding import EmbeddingInizializer
import torch
from preprocessing.dataset import DocumentDataset, StratifiedDocumentDataset
from random import shuffle
from config import (RAW_DATA_DIR, TRAIN_DATA_PATH, VALID_DATA_PATH, TEST_DATA_PATH)


nlp = spacy.load('en')


def nlpPreprocessing(text):
    text = character_corrections(text)
    text = re.sub(r'\\[nt]', r' ', text)
    text = re.sub(r'([".,!?()])', r' \1 ', text)
    text = re.sub(r'[^a-zA-Z",.!?()]+', r' ', text)
    text = text.lower()

    tokens = [str(t) for t in nlp(text) if (t.lemma_ != ' ') and (not t.is_stop)]

    return tokens

def character_corrections(text):
    text = text.replace('&#163;', '£')
    text = text.replace('&#224;', 'à')
    text = text.replace('&#233;', 'é')
    text = text.replace('&#246;', 'ö')
    text = text.replace('&#252;', 'ü')
    text = text.replace('&#235;', 'ë')
    text = text.replace('&#243;', 'ó')
    text = text.replace('&#234;', 'ê')
    text = text.replace('&#232;', 'è')
    text = text.replace('&#220;', 'Ü')
    text = text.replace('&#228;', 'ä')
    text = text.replace('&#189;', '½')
    text = text.replace('&#167;', '§')

    return text


def getDocuments(dir: Path):
    paths = getXmlPaths(dir)

    documents = []
    print('\nLoading documents from xml sources...')
    for p in tqdm(paths):
        documents.append(Document(p))

    return documents


def getXmlPaths(dir: Path):
    paths = []
    for root, dirs, files in os.walk(dir):
        for file in files:
            if file.endswith('.xml'):
                paths.append(root / file)
    return paths


def words_in_glove(words, glove):
    words_in = []
    for w in words:
        if glove.isIn(w):
            words_in.append(w)

    return words_in


def save_vocab_and_embed(documentList):
    glove = Glove.fromFile()

    words = set()
    for doc in documentList:
        for sent in doc.sentences:
            tokens = nlpPreprocessing(sent)
            words = words.union(set(tokens))

    words = list(words)
    words = words_in_glove(words, glove)

    vocabulary = SpecialVocabulary()

    n_variable = len(vocabulary)
    n_fixed = len(words)
    size = glove.embedding_size

    embInit = EmbeddingInizializer(n_variable, n_fixed, size)

    for i, w in enumerate(words):
        emb = glove.embedding(w)
        vocabulary.add_token(w)
        embInit.addEmbedding(i, emb)

    vocabulary.toFile()
    embInit.toFile()


def vectorizeSentTokens(docSentences, vocabulary, eos_idx, bos_idx):
    docidxs = []
    for sent in docSentences:
        sentidxs = [vocabulary.lookup_token(t) for t in sent]
        sentidxs = [bos_idx] + sentidxs + [eos_idx]

        docidxs.append(sentidxs)

    return docidxs


def paddingSentence(sentences, max_len, mask_idx):
    paddedSent = []
    for sent in sentences:
        if len(sent) < max_len:
            offset = max_len - len(sent)

            sent = sent + ([mask_idx] * offset)

        paddedSent.append(sent)

    return paddedSent


def set_document_data(documentList):
    vocabulary = SpecialVocabulary.fromFile()

    mask_idx = vocabulary.lookup_token(mask_token)
    eos_idx = vocabulary.lookup_token(eos_token)
    bos_idx = vocabulary.lookup_token(bos_token)

    documentIdxToSentTokens = []

    print('\nTokenizing documents...')
    for document in tqdm(documentList):

        sentenceTokens = []
        for sentence in document.sentences:
            sentenceTokens.append(nlpPreprocessing(sentence))

        documentIdxToSentTokens.append(sentenceTokens)

    print('\nVectorizing data...')
    for i, document in tqdm(enumerate(documentList)):
        # debug
        labels = document.labels
        sentences = document.sentences

        assert len(labels) == len(sentences)

        sentences = documentIdxToSentTokens[i]
        assert len(labels) == len(sentences)
        sentences = vectorizeSentTokens(sentences, vocabulary, eos_idx, bos_idx)
        assert len(labels) == len(sentences)

        lengths = list(map(lambda s: len(s), sentences))
        max_len = max(lengths)

        rev_sentences = list(map(lambda s: list(reversed(s)), sentences))
        assert len(labels) == len(rev_sentences)

        sentences = paddingSentence(sentences, max_len, mask_idx)
        assert len(labels) == len(sentences)
        rev_sentences = paddingSentence(rev_sentences, max_len, mask_idx)
        assert len(labels) == len(rev_sentences)
        labels = list(map(lambda l: label_to_index[l], document.labels))
        assert len(labels) == len(sentences)

        sentences = torch.LongTensor(sentences)
        rev_sentences = torch.LongTensor(rev_sentences)
        lengths = torch.LongTensor(lengths)
        labels = torch.LongTensor(labels)

        data = (sentences, rev_sentences, lengths, labels)

        document.setData(data)

    return documentList


if __name__ == '__main__':
    documentList = getDocuments(RAW_DATA_DIR)

    save_vocab_and_embed(documentList)
    documentList = set_document_data(documentList)

    shuffle(documentList)

    N_TEST = 4
    N_VAL = 8


    testdata = documentList[:N_TEST]
    valdata = documentList[N_TEST:(N_TEST+N_VAL)]
    traindata = documentList[(N_TEST+N_VAL):]

    testdata = DocumentDataset(testdata)
    valdata = DocumentDataset(valdata)
    traindata = StratifiedDocumentDataset(traindata)

    print(f'\ntrain {len(traindata)} valid {len(valdata)} test {len(testdata)}')

    testdata.save(TEST_DATA_PATH)
    valdata.save(VALID_DATA_PATH)
    traindata.save(TRAIN_DATA_PATH)













