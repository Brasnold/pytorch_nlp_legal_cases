from random import shuffle
from preprocessing.document import Document
import pickle


class DocumentDataset():

    def __init__(self, documents, light=False):
        if light:
            documents = list(map(lambda d: DocumentTrain(d), documents))

        self.items = documents

        self.head = 0
        self.lenght = len(self.items)

    def shuffle(self):
        shuffle(self.items)

    def next(self):
        elem = self.items[self.head]
        self.head = (self.head + 1) % self.lenght

        return elem

    def __len__(self):
        return self.lenght

    def save(self, path):
        with open(path, 'wb') as f:
            pickle.dump(self, f)

    @staticmethod
    def fromFile(path):
        with open(path, 'rb') as f:
            dataset = pickle.load(f)

        return dataset


#for train
class StratifiedDocumentDataset(DocumentDataset):
    def __init__(self, documents):
        self.items = list(map(lambda d: DocumentTrain(d), documents))
        self.lenght = len(self.items)

        self.segments = None
        self.n_segments = None
        self.seg_head = None

    def stratifying(self, segments):
        self.n_segments = segments

        sorted_items = sorted(self.items, key=lambda d: d.seq_len)

        self.segments = chunkIt(sorted_items, self.n_segments)
        self.segments = list(map(lambda s: DocumentDataset(s), self.segments))

        self.seg_head = 0

    def shuffle(self):
        for segment in self.segments:
            segment.shuffle()

    def next(self):
        elem = self.segments[self.seg_head].next()
        self.seg_head = (self.seg_head + 1) % self.n_segments

        return elem

class DocumentTrain():
    def __init__(self, document: Document):
        self.data = document.getData()
        self.name = document.name
        self.path = document.path
        self.seq_len = document.seq_len

    def getData(self):
        return self.data


def chunkIt(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out



