from path import Path
import xml.etree.ElementTree as ET
import re


label_to_color = {'FACT': '#FF4136',               #red
                  'PROCEEDINGS': '#FFDC00',        #yellow
                  'BACKGROUND': '#7FDBFF',         #acqua
                  'FRAMING': '#F012BE',            #fuchsia
                  'DISPOSAL': '#01FF70',           #lime
                  'TEXTUAL': '#DDDDDD',            #gray
                  'OTHER': None
                  }

openhead = '<head>'
closehead = '</head>'
brtag = '<br></br>'


class Document():
    """Read, preprocess and format a document from .xml"""

    def __init__(self, path: Path):

        self.name = path.basename().split('.')[0]
        self.path = path

        self.headers, sentenceData = self.getHeadersAndSentences()

        self.labels = [d[1] for d in sentenceData]
        self.sentences = [d[2] for d in sentenceData]

        self.seq_len = len(self.labels)

        assert len(self.labels) == len(self.sentences)

        self.data = None

    def setData(self, data):
        self.data = data

    def getData(self):
        return self.data

    def toFile(self, path, highlight=None):
        text = self.toHTML(highlight)

        with open(path, 'w') as f:
            f.write(text)

    def toHTML(self, highlight):
        header = self.stringHeaders()
        sentences = self.stringSentences(highlight)
        text = f'{openhead}{header}<br></br>\n{sentences}{closehead}'

        return text

    def stringHeaders(self):
        headers = list(map(lambda h: Document.boldString(h), self.headers))
        headers = list(map(lambda h: Document.centerString(h), headers))
        string = ''.join(headers)
        string = string.replace('\\n', brtag)

        return string

    def stringSentences(self, highlight=None):
        if highlight is None:
            highlight = self.labels

        if highlight !=  False:
            assert len(highlight) == len(self.sentences)

        strings = []
        for i, sent in enumerate(self.sentences):
            if sent[0] == ' ':
                sent = sent[1:]
            if sent[-1] != ' ':
                sent += ' '
            if (sent[-2] not in ('.', '"', ':', '?', '!', ')', '-', ';')) or is_enum(sent):
                br_flag = True
                if (i > 0) and (strings[-1] != brtag):
                    strings.append(brtag)
            else:
                br_flag = False

            if highlight != False:
                strings.append(
                    Document.highlightString(sent, label_to_color[highlight[i]])
                )
            else:
                strings.append(sent)

            if br_flag:
                strings.append(brtag)

        strings = '\n'.join(strings)
        strings = Document.centerString(strings, type='justify')

        return strings

    def getHeadersAndSentences(self):
        xmlNodes = self.getXmlNodes()

        headers = Document.getHeaders(xmlNodes)
        sentences = Document.extractSentences(xmlNodes)

        return headers, sentences


    def parseXml(self):
        with open(self.path, 'r') as file:
            data = file.read()

        tree = ET.fromstring(data)

        return tree

    def getXmlNodes(self):
        return Document.breadthFirstSearch(self.parseXml())

    @staticmethod
    def getHeaders(xmlNodes):
        committee = Document.lookup_tag(xmlNodes, 'committee')
        case = Document.lookup_tag(xmlNodes, 'case')
        date = Document.lookup_tag(xmlNodes, 'date')
        ref = Document.lookup_tag(xmlNodes, 'ref')

        _headers = [committee, case, date, ref]
        headers = []
        for h in _headers:
            if h:
                headers += h
        headers = list(map(lambda h: Document.stringNode(h, remove_escapes=False), headers))

        return headers

    @staticmethod
    def extractSentences(xmlNodes):
        sentenceNodes = [node for node in xmlNodes if node.tag == 'SENT']

        sentences = []
        for node in sentenceNodes:
            text = Document.stringNode(node)
            try:
                label = node.attrib['TYPE']
            except:
                label = 'OTHER'
            id = int(node.attrib['sid'])

            sent = [id, label, text]
            sentences.append(sent)

        sentences.sort(key=lambda e: e[0])

        return sentences

    @staticmethod
    def stringNode(xmlNode, remove_escapes=True):
        text = str(ET.tostring(xmlNode, encoding='utf-8', method='text'))[2:-1]

        text = text.replace('#38;', '')
        text = text.replace("\\'", "\'")
        text = text.replace('~~', '&')

        if remove_escapes:
            text = re.sub(r'\\[nt]', r' ', text)

        return text

    @staticmethod
    def lookup_tag(xmlNodes, tag):
        nodeList = []
        for node in xmlNodes:
            if node.tag == tag:
                nodeList.append(node)

        return nodeList

    @staticmethod
    def breadthFirstSearch(root):
        nodelist = []

        def bfsHelper(r):
            nodelist.append(r)
            for c in r.getchildren():
                bfsHelper(c)

        bfsHelper(root)

        return nodelist

    @staticmethod
    def paragraphString(text):
        return f'<p>{text}</p>'

    @staticmethod
    def boldString(text):
        return f'<b>{text}</b>'

    @staticmethod
    def centerString(text, type='center'):
        return f'<p align="{type}" style="margin:5em;">{text}</p>'

    @staticmethod
    def highlightString(text, color):
        return f'<span style="background-color: {color};">{text}</span>' \
            if color is not None else text


def is_enum(text):
    enumPattern = re.compile(r'^["]?\([0-9a-z]')

    return True if enumPattern.match(text) else False