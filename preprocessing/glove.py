import numpy as np
from config import GLOVE_PATH


class Glove():

    def __init__(self, word_to_index, embeddings):
        self.word_to_index = word_to_index
        self.embeddings = embeddings

        self.embedding_size = embeddings.shape[1]

    def embedding(self, word):
        return self.embeddings[self.word_to_index[word]]

    def isIn(self, word):
        return (word in self.word_to_index)

    @staticmethod
    def fromFile(path=GLOVE_PATH):
        word_to_index = {}
        embeddings = []
        with open(path, 'r') as fp:
            for index, line in enumerate(fp):
                line = line.split(" ")
                word_to_index[line[0]] = index
                embedding_i = np.array([float(val) for val in line[1:]])
                embeddings.append(embedding_i)

        return Glove(word_to_index, np.stack(embeddings))