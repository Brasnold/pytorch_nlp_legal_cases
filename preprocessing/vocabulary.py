import pickle
from constants import unk_token, mask_token, eos_token, bos_token
from config import VOCABULARY_PATH


class Vocabulary():

    def __init__(self, add_unk=True, unk_token=unk_token):

        self._token_to_idx = {}

        self._idx_to_token = {}

        self._add_unk = add_unk

        self.unk_index = -1
        if add_unk:
            self.unk_index = self.add_token(unk_token)

    def add_token(self, token):
        if token in self._token_to_idx:
            index = self._token_to_idx[token]
        else:
            index = len(self._token_to_idx)
            self._token_to_idx[token] = index
            self._idx_to_token[index] = token

        return index

    def lookup_token(self, token):
        if self._add_unk:
            return self._token_to_idx.get(token, self.unk_index)
        else:
            return self._token_to_idx[token]

    def lookup_index(self, index):
        if index not in self._idx_to_token:
            raise KeyError("the index (%d) is not in the Vocabulary" % index)
        return self._idx_to_token[index]

    def __str__(self):
        return f"<Vocabulary(size={len(self)})>"

    def __len__(self):
        return len(self._token_to_idx)

    def toFile(self, path=VOCABULARY_PATH):
        with open(path, 'wb') as f:
            pickle.dump(self, f)

    @staticmethod
    def fromFile(path=VOCABULARY_PATH):
        with open(path, 'rb') as f:
            vocab = pickle.load(f)

        return vocab


class SpecialVocabulary(Vocabulary):
    def __init__(self):
        super().__init__()

        self.add_token(mask_token)
        self.add_token(eos_token)
        self.add_token(bos_token)