import torch
from argparse import Namespace
from preprocessing.dataset import DocumentDataset
from torchmodel.model import BiHLSTM, compute_pred
from torchmodel.torch_utils import to_device
from constants import n_class, index_to_label
from sklearn.metrics import accuracy_score
import numpy as np
import pandas as pd
from config import MODEL_PATH, TEST_DATA_PATH, RESULTS_DIR


args = Namespace(
    sl_layers = 2,
    dl_layers = 2,
    sl_hidden = 200,
    dl_hidden = 200,
)

if not torch.cuda.is_available():
    print("WARNING: cuda isn't available.")
    args.cuda = False
else:
    args.cuda = True

args.device = torch.device('cuda' if args.cuda else 'cpu')

testdata = DocumentDataset.fromFile(TEST_DATA_PATH)

classifier = BiHLSTM(sl_layers=args.sl_layers, dl_layers=args.dl_layers,
                     sl_hidden=args.sl_hidden, dl_hidden=args.dl_hidden,
                     n_class=n_class, dropout=0)

classifier.load_state_dict(torch.load(MODEL_PATH, map_location=args.device))

classifier = classifier.to(args.device)

n_examples = len(testdata)

alltarget = None
allpred = None

classifier.eval()
for i in range(n_examples):
    example = testdata.next()
    data = example.getData()
    x, x_rev, x_len, y = to_device(args.device, *data)

    logits = classifier(x, x_rev, x_len)

    prediction =  compute_pred(logits).cpu().numpy()

    alltarget = np.concatenate([alltarget, y.cpu().numpy()]) if alltarget is not None else y.cpu().numpy()
    allpred = np.concatenate([allpred, prediction]) if allpred is not None else prediction

    prediction = prediction.tolist()

    dir = RESULTS_DIR / example.name
    dir.mkdir_p()

    base_path = dir / example.name + '.html'
    gt_path = dir / example.name + '_gt.html'
    pred_path = dir / example.name + '_pred.html'

    highlight = list(map(lambda i: index_to_label[i], prediction))
    example.toFile(base_path, highlight=False)
    example.toFile(gt_path)
    example.toFile(pred_path, highlight=highlight)

accuracy = accuracy_score(alltarget, allpred)
print(f'\nTest accuracy: {accuracy}\n')

allpred = list(map(lambda i: index_to_label[i], allpred))
alltarget = list(map(lambda i: index_to_label[i], alltarget))

crosstab = pd.DataFrame({'target':alltarget, 'prediction':allpred})
crosstab = pd.crosstab(crosstab.target, crosstab.prediction)

print(crosstab)
