import torch
from torch import nn
from torch.nn import Parameter
from torch.nn import functional as F
from torchmodel.torch_utils import xavier_init
import numpy as np
import pickle
from config import EMBEDDING_INIT_PATH


class SemiFixedEmbedding(nn.Module):

    def __init__(self, embInit):
        super(SemiFixedEmbedding, self).__init__()

        self.size = embInit.size

        self.variable = Parameter(torch.Tensor(embInit.variable))
        self.fixed = torch.Tensor(embInit.fixed)

    def forward(self, input):
        weights = torch.cat([self.variable, self.fixed], 0)

        return F.embedding(input, weights)

    def _to(self, device):
        self.fixed = self.fixed.to(device)

    @staticmethod
    def initFromFile(path=EMBEDDING_INIT_PATH):
        with open(path, 'rb') as f:
            embInit = pickle.load(f)

        return SemiFixedEmbedding(embInit)


class EmbeddingInizializer():

    def __init__(self, n_variable, n_fixed, size):
        self.variable = xavier_init([n_variable, size])
        self.fixed = np.zeros([n_fixed, size], dtype=np.float32)
        self.size = size

    def addEmbedding(self, idx, emb):
        self.fixed[idx, :] = emb

    def toFile(self, path=EMBEDDING_INIT_PATH):
        with open(path, 'wb') as f:
            pickle.dump(self, f)