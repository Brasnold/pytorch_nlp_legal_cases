import torch
from torch import nn
from torch.nn import Parameter
from torch.nn import functional as F
from torchmodel.torch_utils import column_gather, xavier_init
from torchmodel.embedding import SemiFixedEmbedding


class LSTM(nn.Module):

    def __init__(self, n_input, n_output, n_layers, dropout):
        super(LSTM, self).__init__()

        self.rnn = nn.LSTM(n_input, hidden_size=n_output, num_layers=n_layers, batch_first=True, dropout=dropout)

    def forward(self, input, input_lengths=None):

        x, _ = self.rnn(input)

        if input_lengths is not None:
            x = column_gather(x, input_lengths)

        return x


class BiHLSTM(nn.Module):

    def __init__(self, sl_layers, dl_layers, sl_hidden, dl_hidden, n_class, dropout):
        super(BiHLSTM, self).__init__()

        self.dropout = dropout

        self.embedding = SemiFixedEmbedding.initFromFile()

        self.slrnn = LSTM(self.embedding.size, sl_hidden, sl_layers, dropout)
        self.slrnn_rev = LSTM(self.embedding.size, sl_hidden, sl_layers, dropout)
        self.dlrnn = LSTM(sl_hidden*2, dl_hidden, dl_layers, dropout)
        self.dlrnn_rev = LSTM(sl_hidden*2, dl_hidden, dl_layers, dropout)

        self.fw = nn.Linear(dl_hidden * 2, n_class)

        self.eos = Parameter(torch.Tensor(xavier_init([sl_hidden*2])))
        self.bos = Parameter(torch.Tensor(xavier_init([sl_hidden*2])))

    def to(self, device):
        self.embedding._to(device)
        return super().to(device)

    def forward(self, input, input_rev, input_lengths):

        x = self.embedding(input)
        x_rev = self.embedding(input_rev)


        if self.dropout is not None:
            x = F.dropout(x, self.dropout, self.training)
            x_rev = F.dropout(x_rev, self.dropout, self.training)

        x = self.slrnn(x, input_lengths)
        x_rev = self.slrnn_rev(x_rev, input_lengths)

        x = torch.cat([x, x_rev], 1)

        x = F.dropout(x, self.dropout, self.training)

        eos = self.eos[None, :]
        bos = self.bos[None, :]

        x = torch.cat([bos, x, eos], 0)

        x = x[None, :, :]

        inv_idx = torch.arange(x.size(1) - 1, -1, -1).long().to(x.device)
        x_rev = x.index_select(1, inv_idx)

        x = self.dlrnn(x)
        x_rev = self.dlrnn_rev(x_rev)

        x_rev = x_rev.index_select(1, inv_idx)

        x = torch.cat([x, x_rev], 2)

        x = torch.squeeze(x, dim=0)
        x = x[1:-1, :]

        x = F.dropout(x, self.dropout, self.training)

        logits = self.fw(x)

        return logits

    def save_state_dict(self, path):
        torch.save(self.state_dict(), path)


def loss_function(logits, target):
    target = target[:, None]
    cross_entropy = - F.log_softmax(logits, dim=1)
    cross_entropy = torch.gather(cross_entropy, 1, target)

    return torch.sum(cross_entropy)


def compute_prob(logits):
    return F.softmax(logits, dim=1)


def compute_pred(logits):
    return torch.argmax(logits, dim=1)


def compute_accuracy(logits, target):
    pred = torch.argmax(logits, dim=1)
    correct = (pred == target)

    return torch.sum(correct)