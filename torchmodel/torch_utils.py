import torch
import numpy as np


def column_gather(y_out, x_lengths):
    x_lengths = x_lengths.long().detach().cpu().numpy() - 1

    out = []
    for batch_index, column_index in enumerate(x_lengths):
        out.append(y_out[batch_index, column_index])

    return torch.stack(out)


def to_device(dev, *args):
    toreturn = []
    for tensor in args:
        toreturn.append(tensor.to(dev))

    return toreturn


def xavier_init(shape):
    n = shape[-1]
    r = 1 / np.sqrt(n)

    return np.random.uniform(-r, r, shape).astype(np.float32)
