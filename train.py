import torch
from torch import optim
from argparse import Namespace
from preprocessing.dataset import DocumentDataset, StratifiedDocumentDataset
from torchmodel.model import BiHLSTM, compute_accuracy, loss_function
from torchmodel.torch_utils import to_device
from constants import n_class
from math import inf
from config import MODEL_PATH, TRAIN_DATA_PATH, VALID_DATA_PATH


args = Namespace(
    batch_size = 4,
    early_stopping_criteria = 3,
    learning_rate = 1e-03,
    num_epochs = 200,
    sl_layers = 2,
    dl_layers = 2,
    sl_hidden = 200,
    dl_hidden = 200,
    dropout = 0.2,
)

if not torch.cuda.is_available():
    print("WARNING: cuda isn't available.")
    args.cuda = False
else:
    args.cuda = True

args.device = torch.device('cuda' if args.cuda else 'cpu')

traindataset = StratifiedDocumentDataset.fromFile(TRAIN_DATA_PATH)
traindataset.stratifying(args.batch_size)
validdataset = DocumentDataset.fromFile(VALID_DATA_PATH)

classifier = BiHLSTM(sl_layers=args.sl_layers, dl_layers=args.dl_layers,
                     sl_hidden=args.sl_hidden, dl_hidden=args.dl_hidden,
                     n_class=n_class, dropout=args.dropout)

classifier = classifier.to(args.device)

optimizer = optim.Adam(classifier.parameters(), lr=args.learning_rate)

n_examples = len(traindataset)
n_valid_examples = len(validdataset)
n_steps = n_examples * args.num_epochs
batch_per_epoch = n_examples / args.batch_size

if not (n_examples % args.batch_size) == 0:
    print('WARNING: number of examples is not a muliple of batch size.')

epoch = 0

batch_sentences = 0
batch_loss = 0.
batch_accuracy = 0.

epoch_loss = 0.
epoch_accuracy = 0.

best_score = -inf
not_improved_epochs = 0
best_epoch = -1

traindataset.shuffle()
classifier.train()
for i in range(n_steps):
    example = traindataset.next()
    data = example.getData()
    x, x_rev, x_len, y = to_device(args.device, *data)

    logits = classifier(x, x_rev, x_len)

    loss = loss_function(logits, y) * (1 / args.batch_size)

    loss.backward()

    batch_loss += loss.to('cpu').item()

    batch_accuracy += compute_accuracy(logits, y).to('cpu').item()

    batch_sentences += y.size()[0]

    if ((i % args.batch_size) == 0) and (i != 0):
        batch_accuracy /= batch_sentences

        epoch_loss += batch_loss
        epoch_accuracy += batch_accuracy

        optimizer.step()
        optimizer.zero_grad()

        batch_sentences = 0
        batch_loss = 0.
        batch_accuracy = 0.

    if ((i % n_examples) == 0) and (i != 0):
        traindataset.shuffle()

        epoch_loss /= batch_per_epoch
        epoch_accuracy /= batch_per_epoch

        valid_loss = 0.
        valid_accuracy = 0.
        valid_sentences = 0.

        classifier.eval()
        for i in range(n_valid_examples):
            example = validdataset.next()
            data = example.getData()
            x, x_rev, x_len, y = to_device(args.device, *data)

            logits = classifier(x, x_rev, x_len)
            loss = loss_function(logits, y)

            valid_loss += loss.to('cpu').item()
            valid_accuracy += compute_accuracy(logits, y).to('cpu').item()
            valid_sentences += y.size()[0]

        valid_loss /= n_valid_examples
        valid_accuracy /= valid_sentences

        print(f'epoch: {epoch} train-loss: {epoch_loss} train-accuracy: {epoch_accuracy} ' +\
              f'val-loss: {valid_loss} val-accuracy: {valid_accuracy}')

        if  valid_accuracy > best_score:
            best_score = valid_accuracy
            not_improved_epochs = 0
            best_epoch = epoch

            classifier.save_state_dict(MODEL_PATH)
        else:
            not_improved_epochs += 1

        if not_improved_epochs == args.early_stopping_criteria:
            print(f'\nScore not improved for {args.early_stopping_criteria},' +\
                  f' best epoch: {best_epoch} best score: {best_score}')

            break

        epoch_loss = 0.
        epoch_accuracy = 0.

        classifier.train()

        epoch += 1

if not_improved_epochs < args.early_stopping_criteria:
    print(f'\nEarly stopping criteria not matched, best epoch: {best_epoch} best score: {best_score}')






